package main

import "fmt"

func main() {
	const (
		StarHyperGiant = iota
		StarSuperGiant
		StarBrightGiant
		StarGiant
		StarSubGiant
		StarDwarf
		StarSubDwarf
		StarWhiteDwarf
		StarRedDwarf
		StarBrownDwarf
	)

	fmt.Printf("StarHyperGiant\t %v [%T]\n", StarHyperGiant, StarHyperGiant)
	fmt.Printf("StarSuperGiant\t %v [%T]\n", StarSuperGiant, StarSuperGiant)
	fmt.Printf("StarBrightGiant\t %v [%T]\n", StarBrightGiant, StarBrightGiant)
	fmt.Printf("StarGiant\t %v [%T]\n", StarGiant, StarGiant)
	fmt.Printf("StarSubGiant\t %v [%T]\n", StarSubGiant, StarSubGiant)
	fmt.Printf("StarDwarf\t %v [%T]\n", StarDwarf, StarDwarf)
	fmt.Printf("StarSubDwarf\t %v [%T]\n", StarSubDwarf, StarSubDwarf)
	fmt.Printf("StarWhiteDwarf\t %v [%T]\n", StarWhiteDwarf, StarWhiteDwarf)
	fmt.Printf("StarRedDwarf\t %v [%T]\n", StarRedDwarf, StarRedDwarf)
	fmt.Printf("StarBrownDwarf\t %v [%T]\n", StarBrownDwarf, StarBrownDwarf)
}
