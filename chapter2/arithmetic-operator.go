package main

import "fmt"

func main() {
	i, k := 10, 20
	a, b := "one", "twenty"

	fmt.Println("i + k =", i+k)
	fmt.Println("a + b =", a+b)
	fmt.Println("a + + b =", a+" "+b)
}
