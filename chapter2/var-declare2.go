package main

import "fmt"

var name string = "Earth"
var desc string = "Planet"
var radius int = 6378
var mass float64 = 5.972e+24
var active bool = true
var sattelites = []string{
	"Moon",
}

func main() {
	fmt.Println(name, "(", desc, ")")
	// fmt.Println(desc)
	fmt.Println("Radius (km):", radius)
	fmt.Println("Mass (kg):", mass)
	fmt.Println("Is active?", active)
	fmt.Println("Sattelites:", sattelites)
}
