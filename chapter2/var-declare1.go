package main

import "fmt"

var name, desc string
var radius int32
var mass float64
var active bool
var sattelites []string

func main() {
	name = "Sun"
	desc = "Star"
	radius = 685800
	mass = 1.989e+30
	active = true
	sattelites = []string{
		"Mercury",
		"Venus",
		"Earth",
		"Mars",
		"Jupiter",
		"Saturn",
		"Uranus",
		"Neptune",
	}

	fmt.Println(name, "(", desc, ")")
	// fmt.Println(desc)
	fmt.Println("Radius (km):", radius)
	fmt.Println("Mass (kg):", mass)
	fmt.Println("Is active?", active)
	fmt.Println("Sattelites:", sattelites)
}
