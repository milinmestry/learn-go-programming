package main

import "fmt"

var name = "Mars"
var desc = "Planet"
var radius = 3396.2
var mass = 6.4185e+23
var active = true
var sattelites = []string{
	"Phobos",
	"Deimos",
}

func main() {
	fmt.Println(name, "(", desc, ")")
	// fmt.Println(desc)
	fmt.Println("Radius (km):", radius)
	fmt.Println("Mass (kg):", mass)
	fmt.Println("Is active?", active)
	fmt.Println("Sattelites:", sattelites)
}
