package main

import (
	"fmt"
	"net/http"
	"time"
)

type servrMsg string

func (m servrMsg) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Custom-Text", "Gomilin")
	resp.Header().Add("Content-Type", "text/html")
	resp.WriteHeader(http.StatusOK)
	fmt.Fprint(resp, m)
}

func main() {
	msgHandler := servrMsg("Hello from high above 2")
	server := http.Server{
		Addr:         ":1111",
		Handler:      msgHandler,
		ReadTimeout:  time.Second * 5,
		WriteTimeout: time.Second * 3,
	}
	server.ListenAndServe()
}
