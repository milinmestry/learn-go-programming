package http

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
)

func ReadTextHttp16328() {
	url := "https://gutenberg.org/cache/epub/16328/pg16328.txt"
	client := &http.Client{
		Timeout: 21 * time.Second,
	}
	resp, err := client.Get(url)
	// resp, err := http.Get(url)
	if err != nil {
		panic(err)
		// return
	}

	defer resp.Body.Close()

	if resp.StatusCode == 200 {
		// display on console
		io.Copy(os.Stdout, resp.Body)

		// save data to file
		// saveRespBody(resp)
	}
}

func saveRespBody(resp *http.Response) {
	file2, err := os.Create("storage/http-pg16328.txt")
	if err != nil {
		fmt.Println("Unable to open file:", err)
		os.Exit(1)
	}

	n, err := io.Copy(file2, resp.Body)
	if err != nil {
		fmt.Println("Unable to copy file:", err)
		os.Exit(1)
	}
	fmt.Printf("Copied %d bytes to destination file %s\n", n, file2.Name())
}
