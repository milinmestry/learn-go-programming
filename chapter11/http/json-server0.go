package main

// `go get remote package` required to work
import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/vladimirvivien/learning-go/ch11/curr1"
)

// Simple json server for currency service
// As in tcpserver example, returns []curr1.Currency
// This time, however, data is encoded as JSON
// Test with:
// curl -X POST -d '{"get":"Euro"}' http://localhost:1111/currency
var currencies = curr1.Load("storage/input/currencies-data.csv")

func currs(resp http.ResponseWriter, req *http.Request) {
	var currReq curr1.CurrencyRequest
	decJson := json.NewDecoder(req.Body)

	if err := decJson.Decode(&currReq); err != nil {
		resp.WriteHeader(http.StatusBadRequest)
		fmt.Println(err)
		return
	}

	result := curr1.Find(currencies, currReq.Get)
	encJson := json.NewEncoder(resp)

	if err := encJson.Encode(&result); err != nil {
		fmt.Println("ERROR:", err)
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/currency", currs)

	if err := http.ListenAndServe(":1111", mux); err != nil {
		fmt.Println("MAIN ERROR:", err)
	}
}
