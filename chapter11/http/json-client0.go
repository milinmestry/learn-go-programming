package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/vladimirvivien/learning-go/ch11/curr1"
)

func main() {
	var httpPost string = "POST"
	var urlCurrency string = "http://127.0.0.1:1111/currency"
	var param string

	fmt.Print("Look Currency> ")
	_, err := fmt.Scanf("%s", &param)
	// panic(err)

	// encode request
	buf := new(bytes.Buffer)
	currReq := curr1.CurrencyRequest{Get: param}
	err = json.NewEncoder(buf).Encode(currReq)
	if err != nil {
		fmt.Println(err)
		return
	}

	//send request
	client := &http.Client{}
	req, err := http.NewRequest(httpPost, urlCurrency, buf)
	if err != nil {
		fmt.Println(err)
		return
	}

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer resp.Body.Close()

	//decode response
	var currencies []curr1.Currency
	err = json.NewDecoder(resp.Body).Decode(&currencies)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(currencies)
}
