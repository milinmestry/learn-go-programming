package http

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

func SetHttpHeadersNewReq() {
	client := &http.Client{}
	req, err := http.NewRequest(
		"GET", "https://www.rfc-editor.org/rfc/rfc7540.txt", nil,
	)
	if err != nil {
		panic(err)
	}

	req.Header.Add("Accept", "text/plain")
	req.Header.Add("User-Agent", "SampleClient/1.0")

	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	// fmt.Printf("Resonse length is %d", resp.ContentLength)
	if resp.StatusCode == 200 {
		io.Copy(os.Stdout, resp.Body)
	}
	fmt.Printf("Resonse code is %d", resp.StatusCode)
}
