package main

// `go get remote package` required to work
import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"

	"github.com/vladimirvivien/learning-go/ch11/curr1"
)

var currencies = curr1.Load("storage/input/currencies-data.csv")

func currs(resp http.ResponseWriter, req *http.Request) {
	var currReq curr1.CurrencyRequest
	decJson := json.NewDecoder(req.Body)

	if err := decJson.Decode(&currReq); err != nil {
		resp.WriteHeader(http.StatusBadRequest)
		fmt.Println(err)
		return
	}

	result := curr1.Find(currencies, currReq.Get)
	encJson := json.NewEncoder(resp)

	if err := encJson.Encode(&result); err != nil {
		fmt.Println("ERROR:", err)
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func currencyBootstrap(resp http.ResponseWriter, req *http.Request) {
	file, err := os.Open("chapter11/http/currency.html")
	// file, err := os.Open("chapter11/http/tailwind-currency.htm")
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		fmt.Println("GUI ERROR:", err)
		return
	}
	io.Copy(resp, file)
}

func currencyTailwind(resp http.ResponseWriter, req *http.Request) {
	file, err := os.Open("chapter11/http/tailwind-currency.htm")
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		fmt.Println("GUI ERROR:", err)
		return
	}
	io.Copy(resp, file)
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", currencyTailwind)
	mux.HandleFunc("/bootstrap", currencyBootstrap)
	mux.HandleFunc("/currency", currs)

	if err := http.ListenAndServe(":1112", mux); err != nil {
		fmt.Println("MAIN ERROR:", err)
	}
}
