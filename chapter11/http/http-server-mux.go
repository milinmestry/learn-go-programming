package main

import (
	"fmt"
	"net/http"
)

func main() {
	// mux := http.NewServeMux()

	// hello := hello()
	// goodbye := goodbye()

	hello := func(resp http.ResponseWriter, req *http.Request) {
		// resp.Header().Set("Custom-Text", "Gomilin")
		resp.Header().Add("Content-Type", "text/html")
		resp.WriteHeader(http.StatusOK)
		fmt.Fprint(resp, "Hello from Mux!")
	}

	goodbye := func(resp http.ResponseWriter, req *http.Request) {
		// resp.Header().Set("Custom-Text", "Gomilin")
		resp.Header().Add("Content-Type", "text/html")
		resp.WriteHeader(http.StatusOK)
		fmt.Fprint(resp, "Goodbye, It's been real!")
	}

	milin := func(resp http.ResponseWriter, req *http.Request) {
		// resp.Header().Set("Custom-Text", "Gomilin")
		resp.Header().Add("Content-Type", "text/html")
		resp.WriteHeader(http.StatusOK)
		fmt.Fprint(resp, "Hello from Milin Mestry.")
	}

	// mux.HandleFunc("/hello", hello)
	// mux.HandleFunc("/goodbye", goodbye)
	// mux.HandleFunc("/milin", milin)

	// http.ListenAndServe(":1111", mux)

	// Default ServeMux
	http.HandleFunc("/hello", hello)
	http.HandleFunc("/goodbye", goodbye)
	http.HandleFunc("/milin", milin)

	http.ListenAndServe(":1111", nil)

}

// func hello() {
// 	return func(resp http.ResponseWriter, req *http.Request) {
// 		// resp.Header().Set("Custom-Text", "Gomilin")
// 		resp.Header().Add("Content-Type", "text/html")
// 		resp.WriteHeader(http.StatusOK)
// 		fmt.Fprint(resp, "Hello from Mux!")
// 	}
// }

// func goodbye() {
// 	return func(resp http.ResponseWriter, req *http.Request) {
// 		// resp.Header().Set("Custom-Text", "Gomilin")
// 		resp.Header().Add("Content-Type", "text/html")
// 		resp.WriteHeader(http.StatusOK)
// 		fmt.Fprint(resp, "Goodbye, It's been real!")
// 	}
// }
