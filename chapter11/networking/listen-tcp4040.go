package networking

import "net"

func ListenTo4040() {
	listener, err := net.Listen("tcp", ":4040")
	if err != nil {
		panic(err)
	}
	defer listener.Close()

	for {
		conn, err := listener.Accept()
		if err != nil {
			panic(err)
		}
		conn.Write([]byte("Nice to meet you.\n"))
		conn.Close()
	}
}
