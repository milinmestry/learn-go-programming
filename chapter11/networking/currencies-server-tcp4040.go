package networking

import (
	"fmt"
	"net"
	"strings"
	"time"

	// `go get remote package` required to work
	curr "github.com/vladimirvivien/learning-go/ch11/curr0"
)

var currencies = curr.Load("storage/input/currencies-data.csv")

func GoServerSearchCurrencies() {
	ln, err := net.Listen("tcp", ":4042")
	if err != nil {
		panic(err)
	}
	defer ln.Close()

	for {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println(err)
			conn.Close()
			continue
		}

		fmt.Println("Connected to ", conn.RemoteAddr())
		// delegate connection to a goroutine
		go handleConnection(conn)
	}
}

// Handle client connection
func handleConnection(conn net.Conn) {
	defer conn.Close()

	// loop to stay connected with client
	for {
		cmdLine := make([]byte, (1024 * 4))
		n, err := conn.Read(cmdLine)
		if n == 0 || err != nil {
			fmt.Println(err) // This will print at server
			return
		}

		cmd, param := parseCommand(string(cmdLine[0:n]))
		fmt.Printf("command:%s, param:%s\n", cmd, param)

		if cmd == "" {
			continue
		}

		command := strings.ToUpper(cmd)

		if command == "quit" {
			conn.Write([]byte("Bye for now...\n"))
			fmt.Println("User has type QUIT") // This shows on server cli
		}

		// execute command
		switch command {
		case "GET":
			result := curr.Find(currencies, param)
			if len(result) == 0 {
				conn.Write([]byte("Nothing found\n"))
				continue
			}

			// stream result to client
			for _, cur := range result {
				_, err := fmt.Fprintf(
					conn,
					"%s %s [%s] %s\n",
					cur.Name, cur.Code,
					cur.Number, cur.Country,
				)

				if err != nil {
					fmt.Println(err) // This will print at server
					return
				}

				// reset deadline while writing
				// closes conn if client is gone
				conn.SetWriteDeadline(
					time.Now().Add(time.Second * 9))

			}
			conn.Write([]byte("----End result----\n"))

			// reset read deadline for next read
			conn.SetReadDeadline(
				time.Now().Add(time.Second * 300))

		default:
			conn.Write([]byte("invalid command\n"))
			continue
		}
	}
}

func parseCommand(cmdLine string) (cmd, param string) {
	parts := strings.Split(cmdLine, " ")
	if len(parts) != 2 {
		return "", ""
	}
	cmd = strings.TrimSpace(parts[0])
	param = strings.TrimSpace(parts[1])
	return
}
