package networking

import (
	"fmt"
	"io"
	"net"
	"os"
)

func CopySiteDataDialTcp() {
	host, port := "www.gutenberg.org", "80"
	// host, port := "github.com", "80"
	addr := net.JoinHostPort(host, port)

	conn, err := net.Dial("tcp", addr)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	// httpRequest := "GET  /vladimirvivien/learning-go/blob/master/ch11/beowulf.txt HTTP/1.1\n" +
	httpRequest := "GET  /cache/epub/16328/pg16328.txt HTTP/1.1\n" +
		"Host: " + host + "\n\n"
	if _, err := conn.Write([]byte(httpRequest)); err != nil {
		panic(err)
	}

	file, err := os.Create("storage/networking/beowulf.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	// fmt.Printf("%s", conn)
	io.Copy(file, conn)
	fmt.Println("Website data copied to file: ", file.Name())
}
