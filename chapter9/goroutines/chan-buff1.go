package goroutines

import "fmt"

func PrintChanBuffer1() {
	ch := make(chan int, 4)
	ch <- 2
	ch <- 4
	ch <- 6
	ch <- 8

	fmt.Println("-----Buffer channel-----")
	fmt.Println(<-ch)
	fmt.Println(<-ch)
	fmt.Println(<-ch)
	fmt.Println(<-ch)
}
