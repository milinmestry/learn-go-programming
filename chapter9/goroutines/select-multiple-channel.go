package goroutines

import (
	"fmt"
	"strings"
)

func PrintHistogram4() {
	fmt.Println("-----Histogram Demo-4 using select channel-----")

	histogram := make(map[string]int)
	stopCh := make(chan struct{})

	words := getWordForHistogram(stopCh, HistoData())
	for word := range words {
		if histogram["the"] == 3 {
			close(stopCh)
		}
		histogram[word]++ // map overwrites existing key
	}

	PrintHistogramData(histogram)
}

func getWordForHistogram(stopCh chan struct{}, data []string) <-chan string {
	wordCh := make(chan string)

	go func() {
		defer close(wordCh)
		for _, line := range data {
			words := strings.Split(line, " ")
			for _, word := range words {
				word = strings.ToLower(word)
				select {
				case wordCh <- word:
				case <-stopCh:
					return
				}
			}
		}
	}()
	return wordCh
}
