package goroutines

import (
	"fmt"
	"strings"
)

func PrintHistogram3() {
	fmt.Println("-----Histogram Demo-3 using generator channel-----")

	histogram := make(map[string]int)
	words := generatorHistogram(HistoData())
	for word := range words {
		// fmt.Printf("%s | ", word)
		histogram[word]++ // map overwrites existing key
	}

	PrintHistogramData(histogram)
}

func generatorHistogram(data []string) <-chan string {
	wordCh := make(chan string)

	go func() {
		defer close(wordCh)
		for _, line := range data {
			words := strings.Split(line, " ")
			for _, word := range words {
				word = strings.ToLower(word)
				wordCh <- word
			}
		}
	}()
	return wordCh
}
