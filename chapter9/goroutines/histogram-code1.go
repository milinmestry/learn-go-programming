package goroutines

import (
	"fmt"
	"strings"
)

func PrintHistogram1() {
	fmt.Println("-----Histogram Demo-1 using channel-----")

	histogram := make(map[string]int)
	done := make(chan struct{})

	go func() {
		defer close(done)
		// data := HistoData() // array data source
		for _, line := range HistoData() {
			words := strings.Split(line, " ")
			for _, word := range words {
				word = strings.ToLower(word)
				histogram[word]++
			}
		}
	}()

	<-done // block channel until closed

	PrintHistogramData(histogram)
}
