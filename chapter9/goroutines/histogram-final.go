package goroutines

import (
	"fmt"
	"strings"
)

type histogram struct {
	total int
	// atotal [1]int
	// mtotal map[string]int
	freq map[string]int
}

func (h *histogram) ingest() <-chan string {
	out := make(chan string)
	go func() {
		defer close(out)
		for _, line := range HistoData() {
			out <- line
		}
	}()
	return out
}

func (h *histogram) split(in <-chan string) <-chan string {
	out := make(chan string)
	go func() {
		defer close(out)
		for line := range in {
			for _, word := range strings.Split(line, " ") {
				out <- strings.ToLower(word)
			}
		}
	}()
	return out
}

func (h *histogram) count(in <-chan string) chan struct{} {
	done := make(chan struct{})
	go func() {
		defer close(done)
		for word := range in {
			h.freq[word]++
			h.total++ // this always returns 0 outside function when struct is NOT pointer, this is important > *histogram
			// h.mtotal["t"]++
			// h.atotal[0] += 1 // this always returns 0 outside function
			// fmt.Println(h.atotal[0])
		}
	}()
	return done
}

func HistogramFinal() {
	h := histogram{freq: make(map[string]int)}
	done := make(chan struct{})
	go func() {
		defer close(done)
		<-h.count(h.split(h.ingest()))
	}()
	<-done
	PrintHistogramData(h.freq)
	fmt.Printf("Counted %d words!\n", h.total)
	// fmt.Printf("Counted %d words!\n", h.atotal[0])
	// fmt.Printf("Counted %d words!\n", h.mtotal["t"])
}
