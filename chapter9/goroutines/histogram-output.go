package goroutines

import "fmt"

func PrintHistogramData(histogram map[string]int) {
	for k, v := range histogram {
		fmt.Printf("%s\t(%d)\n", k, v)
	}
}
