package goroutines

import "fmt"

func PrintChanUnbuffered1(v int) {
	ch := make(chan int)
	go func(s int) {
		ch <- s
	}(v)
	fmt.Println("-----Unbuffered channel-----")
	fmt.Printf("Channel ch value is %d\n", <-ch)
}
