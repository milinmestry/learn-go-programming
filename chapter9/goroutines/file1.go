package goroutines

import "fmt"

func PrintGoroutines1() {
	// execute within same routine (1)
	// each calls execute serially
	go count(10, 50, 10)
	go count(60, 100, 10)
	go count(110, 200, 20)

	go func() {
		count(211, 255, 11)
	}()

	funcLiteral()

	// end of main()
	// routine 1 exits
	fmt.Scanln()
}

func funcLiteral() {
	// fmt.Println("----function literal----")
	a := []int{400, 700}
	for _, v := range a {
		go func(s int) {
			count(s, s+20, 10)
		}(v)
	}

}

func count(min, max, delta int) {
	for i := min; i <= max; i += delta {
		fmt.Printf("%d ", i)
	}
	fmt.Println()
}
