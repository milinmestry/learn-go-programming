package goroutines

import "fmt"

func PrintChanUniDirect() {
	ch := make(chan int, 10)

	makeEvenNums(4, ch)

	fmt.Println("-----Unidirectional channel-----")
	fmt.Printf("Channel length=%d and capacity=%d\n", len(ch), cap(ch))
	fmt.Println(<-ch)
	fmt.Println(<-ch)
	fmt.Println(<-ch)
	fmt.Println(<-ch)
	fmt.Printf("Channel length=%d and capacity=%d\n", len(ch), cap(ch))
}

func makeEvenNums(count int, in chan<- int) {
	for i := 0; i < count; i++ {
		in <- i * 2
	}
}
