package goroutines

import (
	"fmt"
	"strings"
)

func PrintHistogram2() {
	fmt.Println("-----Histogram Demo-2 using channel-----")

	wordCh := make(chan string)

	go func() {
		defer close(wordCh)
		// data := HistoData() // array data source
		for _, line := range HistoData() {
			words := strings.Split(line, " ")
			for _, word := range words {
				word = strings.ToLower(word)
				wordCh <- word
			}
		}
	}()

	histogram := make(map[string]int)
	// Read each word from channel until channel is not closed
	// for {
	// 	word, opened := <-wordCh
	// 	if !opened {
	// 		break // the loop
	// 	}
	// 	histogram[word]++
	// }

	// Another easy way to read channel data using for range...
	for word := range wordCh {
		histogram[word]++
	}

	PrintHistogramData(histogram)
}
