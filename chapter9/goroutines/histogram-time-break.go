package goroutines

import (
	"fmt"
	"time"
)

func PrintHistogramTimeOut() {
	fmt.Println("-----Histogram Demo-5 using channel Time out-----")

	histogram := make(map[string]int)
	stopCh := make(chan struct{})

	go func() {
		timeS := time.Now()
		defer close(stopCh)
		words := generatorHistogram(HistoData())
		for word := range words {
			histogram[word]++ // map overwrites existing key
		}

		PrintHistogramData(histogram)
		timeE := time.Now()
		elapsed := timeE.Sub(timeS)
		fmt.Printf("Time taken to finish processing is %s\n", elapsed)
	}()

	select {
	case <-stopCh:
		fmt.Println("Read all words!")
	case <-time.After(200 * time.Microsecond):
		fmt.Println("Sorry, Timeout!!!")
	}

}
