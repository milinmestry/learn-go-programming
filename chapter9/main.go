package main

import (
	"fmt"

	"github.com/learning-go-pro-book/chapter6-FooBazz/chapter9/sync"
)

func main() {
	fmt.Println("Main function.")

	// goroutines.PrintGoroutines1()
	// goroutines.PrintChanUnbuffered1(10)
	// goroutines.PrintChanBuffer1()
	// goroutines.PrintChanUniDirect()

	// goroutines.PrintHistogram1()
	// goroutines.PrintHistogram2()
	// goroutines.PrintHistogram3()
	// goroutines.PrintHistogram4()
	// goroutines.PrintHistogramTimeOut()
	// goroutines.HistogramFinal()

	// sync.MainDemo1()
	// sync.MainSyncMutexLock()
	// sync.MainSyncRWMutexLock()
	// sync.PrintSyncWaitGroup()
	// sync.PrintParallelProcess()
	sync.ConcurrentPrintAlphaNum()
}
