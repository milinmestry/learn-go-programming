package sync

import (
	"fmt"
	"sync"
	"time"
)

var MAX int = 10000
var worker int = 10

func PrintSyncWaitGroup() {
	timeS := time.Now()
	values := make(chan int, MAX)
	result := make(chan int, worker)
	var wg sync.WaitGroup
	wg.Add(worker)

	go func() {
		for i := 1; i < MAX; i++ {
			if (i%3) == 0 || (i%5) == 0 {
				values <- i
			}
		}
		close(values)
	}()

	doMyWork := func() {
		defer wg.Done()
		r := 0

		for v := range values {
			r += v
		}
		result <- r
	}

	for i := 0; i < worker; i++ {
		go doMyWork()
	}
	// go doMyWork()
	// go doMyWork()
	// go doMyWork()

	wg.Wait() // wait for groutines

	// total := <-result + <-result + <-result + <-result
	total := 0
	for i := 0; i < worker; i++ {
		total += <-result
	}

	timeE := time.Since(timeS)
	fmt.Printf("Total:%d \t timetaken by workers:%v\n", total, timeE)
}
