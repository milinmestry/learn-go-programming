package sync

import (
	"fmt"
	"time"
)

type Service struct {
	started bool
	stopCh  chan struct{}
}

func (s *Service) Start() {
	s.stopCh = make(chan struct{})
	go func() {
		fmt.Println("----Service started-----")

		s.started = true
		<-s.stopCh // wait to be closed
	}()
}

func (s *Service) Stop() {
	if s.started {
		s.started = false
		close(s.stopCh)
		fmt.Println("----Service stopped-----")
	}
}

func MainDemo1() {
	fmt.Printf("----Sync demo1-----\n")
	s := &Service{}
	s.Start()
	sec := 10
	fmt.Printf("Wait for %d seconds.\n", sec)
	time.Sleep(time.Duration(sec) * time.Second)
	s.Stop()
}
