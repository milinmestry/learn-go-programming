package sync

import (
	"fmt"
	"sync"
	"time"
)

type Service3 struct {
	started bool
	stopCh  chan struct{}
	mutex   sync.RWMutex
	cache   map[int]string
}

func (s *Service3) Start3() {
	s.stopCh = make(chan struct{})
	s.cache = make(map[int]string)

	go func() {
		fmt.Println("----Service started-----")
		s.mutex.Lock()
		s.started = true
		s.cache[0] = "Hello World"
		s.cache[2] = "Hello Galaxy"
		s.cache[3] = "Hello Universe"
		s.mutex.Unlock()
		fmt.Println("----Cache values set-----")
		<-s.stopCh // wait to be closed
	}()
}

func (s *Service3) Stop3() {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	if s.started {
		s.started = false
		close(s.stopCh)
		fmt.Println("----Service stopped-----")
	}
}

func (s *Service3) Serve(id int) {
	// go func() {
	// 	fmt.Printf("id= %d | ", id)
	// 	fmt.Printf("cache %v\n", s.cache)

	// }()
	s.mutex.RLock()
	cacheMsg := s.cache[id]
	s.mutex.RUnlock()
	if cacheMsg != "" {
		fmt.Println(cacheMsg)
	} else {
		fmt.Println("Goodbye!")
	}
}

func MainSyncRWMutexLock() {
	fmt.Printf("\n----Sync RWMutex Lock-----\n")
	s := &Service3{}
	s.Start3()
	fmt.Printf("Is service ready %v \n", s.started)
	time.Sleep(1 * time.Second) // without this the first value is not available
	s.Serve(0)
	time.Sleep(1 * time.Second)
	s.Serve(2)
	time.Sleep(1 * time.Second)
	s.Serve(3)
	s.Stop3()
}
