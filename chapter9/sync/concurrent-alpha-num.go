package sync

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

func ConcurrentPrintAlphaNum() {
	// concurrent
	// runtime.GOMAXPROCS(1)

	// parallel
	runtime.GOMAXPROCS(2)

	var wg sync.WaitGroup
	wg.Add(2)
	fmt.Println("Starting Go Routines")

	go func() {
		defer wg.Done()

		time.Sleep(1 * time.Microsecond)
		for c := 'a'; c < 'a'+26; c++ {
			fmt.Printf("%c ", c)
		}
	}()

	go func() {
		defer wg.Done()

		for i := 0; i < 27; i++ {
			fmt.Printf("%d ", i)
		}
	}()

	fmt.Println("Waiting To Finish")
	wg.Wait()

	fmt.Println("\nTerminating Program.")
}
