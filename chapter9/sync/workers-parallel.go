package sync

import (
	"fmt"
	"sync"
	"time"
)

// var MAX int = 10000
var workers int = 10

func PrintParallelProcess() {
	timeS := time.Now()
	values := make(chan int)
	result := make(chan int, workers)
	var wg sync.WaitGroup

	go func() {
		for i := 1; i < MAX; i++ {
			if (i%3) == 0 || (i%5) == 0 {
				values <- i
			}
		}
		close(values)
	}()

	doMyWork := func() {
		defer wg.Done()
		r := 0

		for v := range values {
			r += v
		}
		result <- r
	}

	wg.Add(workers)
	for i := 0; i < workers; i++ {
		go doMyWork()
	}

	wg.Wait() // wait for groutines
	close(result)
	// total := <-result + <-result + <-result + <-result
	total := 0
	for pr := range result {
		total += pr
	}

	timeE := time.Since(timeS)
	fmt.Printf("Total:%d \t timetaken by workers:%v\n", total, timeE)
}
