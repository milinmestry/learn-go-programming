package sync

import (
	"fmt"
	"sync"
	"time"
)

type Service2 struct {
	started bool
	stopCh  chan struct{}
	// mutex   sync.Mutex
	sync.Mutex
}

func (s *Service2) Start2() {
	s.stopCh = make(chan struct{})
	go func() {
		fmt.Println("----Service started-----")
		// s.mutex.Lock()
		s.Lock()
		s.started = true
		// s.mutex.Unlock()
		s.Unlock()
		<-s.stopCh // wait to be closed
	}()
}

func (s *Service2) Stop2() {
	// s.mutex.Lock()
	s.Lock()
	// defer s.mutex.Unlock()
	defer s.Unlock()
	if s.started {
		s.started = false
		close(s.stopCh)
		fmt.Println("----Service stopped-----")
	}
}

func MainSyncMutexLock() {
	fmt.Printf("\n----Sync Mutex Lock-----\n")
	s := &Service2{}
	s.Start2()
	sec := 10
	fmt.Printf("Wait for %d seconds.\n", sec)
	time.Sleep(time.Duration(sec) * time.Second)

	s2 := &Service2{}
	s2.Start2()
	sec = 5
	fmt.Printf("Wait for %d seconds.\n", sec)
	time.Sleep(time.Duration(sec) * time.Second)
	s2.Stop2()

	s.Stop2()
}
