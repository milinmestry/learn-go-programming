package main

import (
	"fmt"

	encodedecode "github.com/learning-go-pro-book/chapter6-FooBazz/chapter10/encode-decode"
)

// milinmestry@gmail.com
func main() {
	fmt.Println("Main function.")

	// iodemo.PrintIt()
	// iodemo.PrintSourceAlphaReader()
	// iodemo.PrintFileAlphaReader()
	// iodemo.PrintChannelWriter()
	// iodemo.PrintChannelWriter2()
	// iodemo.PrintChannelReaderWriter()
	// var file1 = "storage/text1.md"
	// iodemo.CopyFile(file1)
	// iodemo.FileBackDemo()
	// iodemo.WriteStringToFile()
	// iodemo.WriteBytesToFile()
	// iodemo.FileReadRawBytes()
	// iodemo.DemoOsStdout()

	// fmt.Println("\n~~~~~IO Format~~~~~~")
	// formattedio.FPrintfDataFile()
	// formattedio.FPrintfCsvDataFile()
	// formattedio.ReadPlanetsFile()
	// formattedio.WhatIsSquareCalled()

	// fmt.Println("\n~~~~~BUFIO Format~~~~~~")
	// buffio.WriteDataToFile()
	// buffio.ReadFileBufioScanner()
	// buffio.InMemoryBytesBuffer()

	fmt.Println("\n~~~~~ENCDE/DECODE~~~~~~")
	// encodedecode.EncodeBooksData()
	// encodedecode.DecodeBooks()
	// encodedecode.EncodeBooksJsonData()
	// encodedecode.BooksJsonTag()
	encodedecode.JsonMarshalEncodeData()
}
