package encodedecode

import (
	"encoding/gob"
	"fmt"
	"os"
)

func DecodeBooks() {
	file, err := os.Open("storage/encodeco/books.data")
	if err != nil {
		fmt.Println("Unable to open file books.data", err)
		os.Exit(2)
	}
	dec := gob.NewDecoder(file)

	var books []Book
	if err := dec.Decode(&books); err != nil {
		panic(err)
	}

	fmt.Println(books)
}
