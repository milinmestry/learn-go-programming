package encodedecode

import (
	"encoding/json"
	"fmt"
	"os"
	"time"
)

type Publisher struct {
	PubName string `json:"name,omniempty"`
}

type JBook struct {
	Title     string `json:"book_title"`
	PageCount int    `json:"pages,string"`
	ISBN      string `json:"isbn"`
	Authors   []Name `json:"authors,omniempty"`
	// Publisher   string    `json:"publisher,omniempty"`
	Publisher   `json:"publisher,omniempty"`
	PublishDate time.Time `json:"publish_date"`
}

// This didn't work as expected, only return Publisher data in JSON
// func (p *Publisher) MarshalJSON() ([]byte, error) {
// 	fmt.Printf("%v\n", b)
// 	fmt.Println("Marshal Publisher")
// 	return []byte(fmt.Sprintf("\"%s Inc.\"", p.PubName)), nil
// }

func (n *Name) MarshalJSON() ([]byte, error) {
	fmt.Println("Marshal Name")
	// return []byte(fmt.Sprintf("\"%s %s\"", n.Last, n.First)), nil
	return []byte("\"" + n.GetFullName() + "\""), nil
}

func (n *Name) GetFullName() string {
	return n.Last + " " + n.First
}

func (jb JBook) MarshalJSON() ([]byte, error) {
	fmt.Println("Marshal JBook")
	type AJB JBook
	jj := AJB(jb)
	// inc := " Inc."
	jj.Publisher.PubName += " Inc."
	return json.Marshal(jj)
}

func JsonMarshalEncodeData() {
	books := []JBook{
		JBook{
			Title:       "Leaning Go",
			PageCount:   375,
			ISBN:        "9781784395438",
			Authors:     []Name{{"Vladimir", "Vivien"}},
			Publisher:   Publisher{"Packt"},
			PublishDate: time.Date(2016, time.July, 0, 0, 0, 0, 0, time.UTC),
		},
		JBook{
			Title:       "The Go Programming Language",
			PageCount:   380,
			ISBN:        "9780134190440",
			Authors:     []Name{{"Alan", "Donavan"}, {"Brian", "Kernighan"}},
			Publisher:   Publisher{"Addison-Wesley"},
			PublishDate: time.Date(2015, time.October, 26, 0, 0, 0, 0, time.UTC),
		},
		JBook{
			Title:       "Introducing Go",
			PageCount:   124,
			ISBN:        "978-1491941959",
			Authors:     []Name{{"Caleb", "Doxsey"}},
			Publisher:   Publisher{"O'Reilly"},
			PublishDate: time.Date(2016, time.January, 0, 0, 0, 0, 0, time.UTC),
		},
	}

	file, err := os.Create("storage/encodeco/books1.json")
	if err != nil {
		fmt.Println("Unable to create file", err)
		os.Exit(2)
	}

	enc := json.NewEncoder(file)

	if err := enc.Encode(books); err != nil {
		fmt.Println("Encode error", err)
		os.Exit(2)
	}
	fmt.Println("All Books data encoded in JSON using MarshalJSON")
}
