package iodemo

import (
	"fmt"
	"io"
	"os"
)

func FileBackDemo() {
	file1, err := os.Open("storage/text1.md")
	if err != nil {
		fmt.Println("Unable to open file:", err)
		os.Exit(1)
	}

	file2, err := os.Create("storage/backup-text1.txt")
	if err != nil {
		fmt.Println("Unable to open file:", err)
		os.Exit(1)
	}

	n, err := io.Copy(file2, file1)
	if err != nil {
		fmt.Println("Unable to copy file:", err)
		os.Exit(1)
	}
	fmt.Printf("Copied %d bytes from source file %s to destination file %s\n", n, file1.Name(), file2.Name())
}
