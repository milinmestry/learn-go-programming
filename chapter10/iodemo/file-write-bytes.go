package iodemo

import (
	"fmt"
	"os"
)

func WriteBytesToFile() {
	fmt.Printf("\n~~~File Write bytes Demo~~~\n")

	rows := [][]byte{
		[]byte("The quick brown fox "),
		[]byte("jumps over the lazy dog\n"),
		[]byte("世界您好 हैलो वर्ल्ड привет мир Merhaba Dünya"),
	}

	file1, err := os.Create("storage/file-write-bytes.txt")
	if err != nil {
		fmt.Println("Unable to open file:", err)
		os.Exit(1)
	}
	defer file1.Close()

	for _, row := range rows {
		file1.Write(row)
	}

	fmt.Printf("File Write bytes completed.\n")

}
