package iodemo

import (
	"fmt"
	"io"
	"os"
)

func PrintFileAlphaReader() {
	fmt.Printf("\n~~~File Reader Demo~~~\n")
	// Filepath is relative to root directory of project
	file, _ := os.Open("storage/text1.md")
	// fmt.Printf("file %s\n", ferr)
	alpha := SourceAlphaReader(file)
	_, err := io.Copy(os.Stdout, alpha)
	if err != nil {
		fmt.Println("Error:", err)
	}
	fmt.Println()
}
