package iodemo

import "fmt"

type channelWriter chan byte

func (c channelWriter) Write(p []byte) (int, error) {
	if len(p) == 0 {
		return 0, nil
	}
	wait := make(chan struct{})
	count := 0
	go func() {
		for _, b := range p {
			c <- b
			count++
		}
		close(c)
		close(wait)
	}()
	<-wait
	return count, nil
}

func PrintChannelWriter() {
	fmt.Printf("\n~~~Simple Write with Channel Demo~~~\n")
	data := []byte("Stream me again!")
	cw := channelWriter(make(chan byte, len(data)))
	cw.Write(data)
	for c := range cw {
		fmt.Print(string(c))
		// fmt.Printf("%c", c)
	}
}
