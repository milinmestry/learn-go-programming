package iodemo

import (
	"fmt"
	"io"
	"os"
)

func FileReadRawBytes() {
	fmt.Printf("\n~~~File Read raw bytes Demo~~~\n")

	file1, err := os.Open("storage/file-write-bytes.txt")
	if err != nil {
		fmt.Println("Unable to open file:", err)
		os.Exit(1)
	}
	defer file1.Close()

	p := make([]byte, 1024)
	for {
		n, err := file1.Read(p)
		if err == io.EOF {
			break
		}
		fmt.Print(string(p[:n]))
		// fmt.Printf("%s", p[:n])
	}
}
