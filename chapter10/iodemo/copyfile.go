package iodemo

import (
	"fmt"
	"io"
	"os"
)

func CopyFile(file string) {
	fmt.Printf("\n~~~Copy file Demo~~~\n")

	fmt.Println("Given filename is >>>>", file)
	if len(file) == 0 {
		panic("Please provide file name.")
	}

	f1, err := os.Open(file)
	if err != nil {
		fmt.Println("Unable to open file:", err)
		os.Exit(1)
	}
	defer f1.Close()

	n, err := io.Copy(os.Stdout, f1)
	if err != nil {
		fmt.Println("Failed to copy:", err)
		os.Exit(1)
	}

	fmt.Printf("\nCopied %d bytes from %s \n", n, f1.Name())
}
