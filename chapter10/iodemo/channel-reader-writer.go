package iodemo

import (
	"fmt"
	"io"
	"os"
)

func PrintChannelReaderWriter() {
	fmt.Printf("\n~~~Channel Reader Writer Demo~~~\n")
	cw := NewChannelWriter()

	file, err := os.Open("storage/text1.md")
	if err != nil {
		panic(err)
		// os.Exit(1)
	}

	_, err = io.Copy(cw, file)
	if err != nil {
		panic(err)
		// os.Exit(1)
	}

	for c := range cw.Channel {
		fmt.Printf("%c", c)
	}

}
