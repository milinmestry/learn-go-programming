package iodemo

import (
	"fmt"
	"io"
	"os"
	"strings"
)

type sourceAlphaReader struct {
	source io.Reader
}

func SourceAlphaReader(source io.Reader) *sourceAlphaReader {
	return &sourceAlphaReader{source}
}

func PrintSourceAlphaReader() {
	str := strings.NewReader("<?_Hello!!! Where is the Sunder?>")
	alpha := SourceAlphaReader(str)
	io.Copy(os.Stdout, alpha)
	fmt.Println()
}

func (a *sourceAlphaReader) Read(p []byte) (int, error) {
	if len(p) == 0 {
		return 0, nil
	}

	count, err := a.source.Read(p) // p has now source data

	if err != nil {
		return count, err
	}

	for i := 0; i < len(p); i++ {
		if (p[i] >= 'A' && p[i] <= 'Z') ||
			(p[i] >= 'a' && p[i] <= 'z') {
			continue
		} else {
			p[i] = 0
		}
	}
	return count, io.EOF
}
