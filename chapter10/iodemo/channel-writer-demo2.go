package iodemo

import "fmt"

func PrintChannelWriter2() {
	fmt.Printf("\n~~~Channel Writer Demo 2~~~\n")
	cw := NewChannelWriter()

	go func() {
		fmt.Fprint(cw, "Channel writer stream!!")
	}()

	for c := range cw.Channel {
		fmt.Printf("%c", c)
	}

}
