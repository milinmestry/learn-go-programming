package iodemo

import (
	"fmt"
	"os"
)

func WriteStringToFile() {
	fmt.Printf("\n~~~File Write strings Demo~~~\n")

	rows := []string{
		"The quick brown fox ",
		"jumps over the lazy dog",
	}

	file1, err := os.Create("storage/file-write-strings.txt")
	if err != nil {
		fmt.Println("Unable to open file:", err)
		os.Exit(1)
	}
	defer file1.Close()

	for _, row := range rows {
		file1.WriteString(row)
	}

	fmt.Printf("File Write strings completed.\n")

}
