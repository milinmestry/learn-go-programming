package iodemo

import (
	"fmt"
	"io"
	"os"
)

func DemoOsStdout() {
	fmt.Printf("\n~~~os.Stdout Demo~~~\n")

	f1, err := os.Open("storage/text1.md")
	if err != nil {
		fmt.Println("Unable to open file:", err)
		os.Exit(1)
	}
	defer f1.Close()

	n, err := io.Copy(os.Stdout, f1)
	if err != nil {
		fmt.Println("Error during copy file data:", err)
		os.Exit(1)
	}
	fmt.Printf("\nToal %d bytes copied from file %s\n", n, f1.Name())
}
