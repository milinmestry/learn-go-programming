package buffio

import (
	"bytes"
	"os"
)

func InMemoryBytesBuffer() {
	var books bytes.Buffer
	books.WriteString("The Great Gatsby")
	books.WriteString("1984")
	books.WriteString("A Tale of Two Cities")

	books.WriteTo(os.Stdout)

	// Create new file and write data in it
	// books.WriteTo(file)
}
