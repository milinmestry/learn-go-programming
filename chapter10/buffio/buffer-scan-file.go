package buffio

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func ReadFileBufioScanner() {
	// filepath := "storage/metalloids.csv";
	filepath := "storage/input/planets.txt"
	file, err := os.Open(filepath)

	if err != nil {
		panic(err)
	}
	fmt.Printf("%-10s %-10s %-6s %-6s\n",
		"Planet", "Diameter", "Moons", "Ring?",
	)
	fmt.Printf("%-10s %-10s %-6s %-6s\n",
		"-----", "-----", "-----", "-----",
	)

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		fields := strings.Split(scanner.Text(), " ")
		fmt.Printf("%-10s %-10s %-6s %-6s\n",
			fields[0], fields[1], fields[2], fields[3],
		)
	}
}
