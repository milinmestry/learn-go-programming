package buffio

import (
	"bufio"
	"fmt"
	"os"
)

func WriteDataToFile() {
	rows := []string{
		"The quick brown fox ",
		"jumps over the lazy dog",
	}

	var filepath = "storage/bufio/write-str.txt"
	file, err := os.Create(filepath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	writer := bufio.NewWriter(file)
	for _, row := range rows {
		writer.WriteString(row)
	}
	writer.Flush()
	fmt.Println("File created")
}
