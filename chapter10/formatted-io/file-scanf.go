package formattedio

import (
	"fmt"
	"io"
	"os"
)

func ReadPlanetsFile() {
	var name, hasString string
	var diam, moon int

	inputData, err := os.Open("storage/input/planets.txt")
	if err != nil {
		fmt.Println("Uanble to open file.", err)
		os.Exit(1)
	}
	defer inputData.Close()

	// header
	fmt.Printf("%-10s %-10s %-6s %-6s\n",
		"Planet", "Diameter", "Moons", "Ring?",
	)
	fmt.Printf("%-10s %-10s %-6s %-6s\n",
		"-----", "-----", "-----", "-----",
	)

	for {
		_, err := fmt.Fscanf(
			inputData,
			"%s %d %d %s\n",
			&name, &diam, &moon, &hasString,
		)

		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Println("Scan file error:", err)
				os.Exit(1)
			}
		}

		fmt.Printf("%-10s %-10d %-6d %-6s\n", name, diam, moon, hasString)
	}
}
