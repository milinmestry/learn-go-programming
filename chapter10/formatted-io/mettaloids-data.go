package formattedio

type metalloid struct {
	name   string
	number int32
	weight float64
}

func MetalloidGetdata() []metalloid {
	return []metalloid{
		{"Boron", 9, 10.81},
		{"Silicon", 14, 28.085},
		{"Germanium", 32, 74.63},
		{"Arsenic", 33, 74.921},
		{"Antimony", 51, 121.760},
		{"Tellerium", 52, 127.60},
		{"Polonium", 84, 209.0},
	}
}
