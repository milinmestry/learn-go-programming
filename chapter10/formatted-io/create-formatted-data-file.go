package formattedio

import (
	"fmt"
	"os"
)

func FPrintfDataFile() {
	var filepath = "storage/metalloids.txt"
	file, err := os.Create(filepath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	for _, m := range MetalloidGetdata() {
		fmt.Fprintf(
			file,
			"%-15s %-10d %-10.3f\n",
			m.name, m.number, m.weight,
		)
	}
	fmt.Println("Data written to the file", filepath)
}

func FPrintfCsvDataFile() {
	var filepath = "storage/metalloids.csv"
	file, err := os.Create(filepath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	// csv headers
	fmt.Fprintf(
		file,
		"Name, Number, Weight\n",
	)
	// csv data
	for _, m := range MetalloidGetdata() {
		fmt.Fprintf(
			file,
			"'%s', '%d', '%.3f'\n",
			m.name, m.number, m.weight,
		)
	}
	fmt.Println("CSV Data written to the file", filepath)
}
