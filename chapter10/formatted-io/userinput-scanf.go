package formattedio

import "fmt"

func WhatIsSquareCalled() {
	var choice int32

	fmt.Println("A Square is called?")
	fmt.Print("Enter 1=quadrilateral 2=rectagonal:")

	n, err := fmt.Scanf("%d", &choice)
	if n != 1 || err != nil {
		fmt.Println("Follow directions!", err)
		return
	}

	if choice == 1 {
		fmt.Println("you answer is correct.")
	} else {
		fmt.Println("you answer is incorrect, try again.")
	}

}
