package vector

import "testing"

func TestNewVector(t *testing.T) {
	v := New(1, 2, 4)
	if len(v) != 3 {
		t.Errorf("Expecting vector size %d but got %d", 3, len(v))
	}
}

func TestVectorString(t *testing.T) {
	s := New(1, 2, 3, 4)
	es := "[1,2,3,4]"
	if s.String() != es {
		t.Logf("Expecting %s, but got %s", es, s)
		t.Fail()
	}
}
