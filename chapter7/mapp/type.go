package mapp

import "fmt"

func PrintType() {
	var (
		histogram map[string]int = map[string]int{
			"Jan": 201, "Feb": 101, "Mar": 301, "Apr": 401,
		}
		table map[string][]int = map[string][]int{
			"Men":   {1, 2, 3},
			"Women": {11, 12, 13},
		}
	)

	fmt.Printf("histogram [%T]\t%v\n", histogram, histogram)
	fmt.Printf("table [%T]\t%v\n", table, table)
}
