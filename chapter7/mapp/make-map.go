package mapp

import "fmt"

func MakeMap() {
	hist := make(map[string]int)
	hist["jan"] = 1
	hist["feb"] = 2
	hist["mar"] = 3
	hist["apr"] = 4

	fmt.Printf("Length of hist map = %d\n", len(hist))
	fmt.Printf("hist map = %v\n", hist)
}
