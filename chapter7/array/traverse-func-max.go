package array

import (
	"math/rand"
	"time"
)

const size = 10
const variance = 10000

var NumsArr [size]int

func init() {
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < size; i++ {
		NumsArr[i] = rand.Intn(variance)
	}
}

func MaxNum(NumsArr [size]int) int {
	temp := NumsArr[0]
	for _, val := range NumsArr {
		if val > temp {
			temp = val
		}
	}
	return temp
}
