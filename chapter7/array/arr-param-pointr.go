package array

import (
	"math/rand"
	"time"
)

type NumsPtrArr [1024 * 1024]int

func InitArr(nums *NumsPtrArr) {
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < size; i++ {
		nums[i] = rand.Intn(variance)
	}
}

func MaxNumPtrArr(nums *NumsPtrArr) int {
	temp := nums[0]
	for _, val := range nums {
		if val > temp {
			temp = val
		}
	}
	return temp
}
