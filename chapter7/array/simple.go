package array

import "fmt"

var days [7]string = [7]string{
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
	"Sunday",
}

var weekdays = [...]string{
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
}

// 2 = rows
// 4 = columns
// 3 = set
// 5 = values in set
var matrix = [2][4][3][5]byte{
	{
		{{1, 2, 3}, {4, 5, 6}},
		{{55, 12}, {22, 4}, {17, 18, 19, 20, 21}},
	},
	{
		{{101, 102}, {111, 112}, {113, 114}},
		{{201, 202}, {211, 212}},
		{{221, 222}, {231, 232}},
		{{241, 242}, {251, 255}},
	},
}

func PrintMe() {
	fmt.Printf("%T: %v\n", days, days)
	fmt.Printf("%T: %v\n", weekdays, weekdays)
	fmt.Printf("%T: %v\n", matrix, matrix)
	fmt.Printf("matrix %T: cap(%d) and len(%d)\n", matrix, len(matrix), cap(matrix))

	fmt.Println("Another Matrixx name type-")
	var mat1 matrixx = initMatx()
	fmt.Println(mat1)
}
