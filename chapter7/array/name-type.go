package array

type matrixx [2][2][2][2]byte

func initMatx() matrixx {
	return matrixx{
		{{{4, 4}, {3, 3}}, {{44, 44}, {33, 33}}},
		{{{14, 14}, {31, 31}}, {{144, 144}, {133, 133}}},
	}
}
