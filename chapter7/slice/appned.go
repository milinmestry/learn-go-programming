package slice

import "fmt"

func AppendSlice() {
	categories := make([]string, 3, 4)
	categories = append(categories, "cat1", "cat2", "cat3",
		"cat4", "cat5", "cat6")
	categories = append(categories, []string{"cat7", "cat8", "cat9"}...) // ... is important here

	fmt.Printf("categories [%d,%d]--> %v\n", len(categories), cap(categories), categories)
}
