package slice

import "fmt"

var (
	months = [12]string{
		"Jan", "Feb", "Mar",
		"Apr", "May", "Jun",
		"Jul", "Aug", "Sep",
		"Oct", "Nov", "Dec",
	}

	mq1 = months[0:3]
	mq2 = months[3:6]
	mq3 = months[6:9]
	mq4 = months[9:]
)

func PrintSliceArr() {
	fmt.Printf("Full Months [%T]\t%v\n", months, months)
	fmt.Printf("Months Q1 [%T]\t%v\n", mq1, mq1)
	fmt.Printf("Months Q2 [%T]\t%v\n", mq2, mq2)
	fmt.Printf("Months Q3 [%T]\t%v\n", mq3, mq3)
	fmt.Printf("Months Q4 [%T]\t%v\n", mq4, mq4)

	// fmt.Printf("&months address\t%v\n", &months)
	// fmt.Printf("&mq1 address\t%v\n", &mq1)
	// fmt.Printf("&mq4 address\t%v\n", &mq4)

}
