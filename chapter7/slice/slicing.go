package slice

import "fmt"

var (
	halfYr = []string{
		"Jan", "Feb", "Mar",
		"Apr", "May", "Jun",
	}

	q1  = halfYr[:3]
	q2  = halfYr[3:]
	mam = halfYr[2:5]
)

func PrintMe() {
	fmt.Printf("halfYr [%d,%d]--> %v\n", len(halfYr), cap(halfYr), halfYr)
	fmt.Printf("q1 [%d,%d]--> %v\n", len(q1), cap(q1), q1)
	fmt.Printf("q2 [%d,%d]--> %v\n", len(q2), cap(q2), q2)
	fmt.Printf("Mar/Apr/May [2:5]--> %v\n", mam)
}
