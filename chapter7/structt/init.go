package structt

import "fmt"

func PrinInit() {
	var (
		// empty = struct{}{}
		car      = struct{ make, model string }{make: "Ford", model: "T555"}
		currency = struct {
			name, country string
			code          int
		}{
			"USD", "United States",
			840,
		}
		node = struct {
			edges  []string
			weight int
		}{
			edges: []string{"north", "south", "west"},
		}

		person = struct {
			name    string
			address struct {
				street      string
				city, state string
				postal      string
			}
		}{
			name: "Tim the Robot",
			address: struct {
				street      string
				city, state string
				postal      string
			}{street: "Main Street"},
		}
		// {
		// 	name:	"Milin M",
		// 	address: struct {
		// 		street: "rasta",
		// 		city: "Mumbai",
		// 		state: "MH",
		// 		postal: "33333"
		// 	}
		// }
	)

	fmt.Printf("car %T\n", car)
	fmt.Printf("currency %T\n", currency)
	fmt.Printf("node %T\n", node)
	fmt.Printf("person %T\n", person)
}
