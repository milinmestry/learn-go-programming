package structt

import (
	"encoding/json"
	"fmt"
)

type (
	Person struct {
		Name    string  `json:"person_name"`
		Title   string  `json:"person_title"`
		Address Address `json:"person_address"`
	}

	Address struct {
		Street string `json:"person_address_street"`
		City   string `json:"person_address_city"`
		State  string `json:"person_address_state"`
		Postal string `json:"person_address_postal"`
	}
)

func PrintPersonJson() {
	jp := Person{
		Name:  "Bhargavi Mestry",
		Title: "Princess Japan",
		Address: Address{
			Street: "S V Road",
			City:   "Bombay",
			State:  "MH",
			Postal: "38382",
		},
	}

	b, err := json.Marshal(jp)
	if err != nil {
		panic(err.Error())
	}
	fmt.Println(jp)
	// fmt.Println(b)
	fmt.Println(string(b))
}
