package structt

import "fmt"

type (
	person struct {
		name    string
		address address
	}

	address struct {
		street      string
		city, state string
		postal      string
	}
)

func MakePerson() person {
	addrss := address{
		street: "SV road",
		city:   "Mumbai",
		state:  "MH",
		postal: "939393",
	}

	return person{
		name:    "Milin M",
		address: addrss,
	}
}

func PrintPerson(p person) {
	fmt.Println(p)
}
