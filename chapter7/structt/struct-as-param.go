package structt

import "fmt"

type (
	perrson struct {
		name    string
		title   string
		address addrress
	}

	addrress struct {
		street      string
		city, state string
		postal      string
	}
)

func updateName(p perrson, name string) perrson {
	p.name = name
	return p // without this updated value will not be available outside the function
}

func updateNamePtr(p *perrson, name string) {
	p.name = name
}

func PrintIt() {
	pr := perrson{}
	// pr := new(perrson)
	pr.name = "not defined"
	pr.title = "author"
	pr.address.street = "12232 Main street"
	pr.address.city, pr.address.state = "Govilleg", "Gola"
	pr.address.postal = "12311"
	fmt.Printf("Person before updateName() %v\n", pr)
	pr = updateName(pr, "Mestry Milin")
	fmt.Printf("Person after updateName() %v\n", pr)

	fmt.Println("------Struct as Param Pointer-----")
	p := new(perrson)
	p.name = "uknown"
	p.title = "author"
	p.address.street = "12345 Main street"
	p.address.city, p.address.state = "Goville", "Go"
	p.address.postal = "12345"
	fmt.Printf("Person before updateNamePtr() %v\n", p)
	updateNamePtr(p, "Vladimir Vivien")
	fmt.Printf("Person after updateNamePtr() %v\n", p)
}
