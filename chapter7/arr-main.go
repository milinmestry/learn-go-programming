package main

import (
	"fmt"

	"github.com/learning-go-pro-book/chapter6-FooBazz/chapter7/array"
	"github.com/learning-go-pro-book/chapter6-FooBazz/chapter7/mapp"
	"github.com/learning-go-pro-book/chapter6-FooBazz/chapter7/slice"
	"github.com/learning-go-pro-book/chapter6-FooBazz/chapter7/structt"
)

func main() {
	fmt.Printf("Weekdays\n")
	array.PrintMe()
	fmt.Println("New random Max number=", array.MaxNum(array.NumsArr))

	var nums *array.NumsPtrArr = new(array.NumsPtrArr)
	array.InitArr(nums)
	// fmt.Println(nums)
	fmt.Printf("Max number from array pointer is %d\n", array.MaxNumPtrArr(nums))

	fmt.Println("\n-----Slice Type-----")
	slice.PrintMe()
	slice.PrintSliceArr()
	slice.MakeSlice()
	slice.AppendSlice()
	slice.SliceUsage()
	fmt.Println("\n\n-----Map Type-----")
	mapp.PrintType()
	mapp.MakeMap()
	mapp.UseMap()
	fmt.Println("\n\n-----Struct Type-----")
	structt.PrinInit()
	fmt.Println()
	structt.PrintPerson(structt.MakePerson())
	fmt.Println()
	structt.PrintPlanet()
	fmt.Println()
	structt.PrintIt()
	fmt.Println()
	structt.PrintPersonJson()
}
