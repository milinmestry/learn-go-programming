// Testing golang godoc exmaple
// Calculates sum of all multiple of 3 and 5 less than MAX
// Source: //projectecluer.net/problem=1
package main

import "fmt"

const MAX = 100000000

func main() {
	work := make(chan int, MAX)
	result := make(chan int, 0)

	// 1. Look for number and add them to work
	// using go coroutine
	go func() {
		for i := 1; i < MAX; i++ {
			if (i%3 == 0) || (i%5 == 0) {
				work <- i // push for work
			}
		}
		close(work)
	}()

	// 2. Sum the number from channel work
	// using go coroutine
	go func() {
		r := 0
		for i := range work {
			r += i
		}
		result <- r
	}()

	fmt.Println("Euler [page#20] ~ Calculate sum of all multiple of 3 or 5") // this will print immediately
	fmt.Println("MAX is:", MAX)                                              // this will print immediately
	// 3. wait for work to complete and then print result
	fmt.Println("Sum is:", <-result)

}
