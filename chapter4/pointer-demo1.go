package main

import (
	"fmt"
	"math"
)

func NewBigNumber() *float64 {
	big := math.MaxFloat64
	return &big
}

func half(val *float64) {
	*val = *val / 2
}

func double(val float64) {
	val = val * 2
}

func main() {
	bigNumber := NewBigNumber()
	fmt.Println("BigNumber =", *bigNumber)
	fmt.Printf("bigNumber address\t%v\n", bigNumber)
	fmt.Printf("&bigNumber address\t%v\n", &bigNumber)
	half(bigNumber)
	// half(bigNumber)
	fmt.Printf("Half BigNumber\t%v\n", *bigNumber)
	double(*bigNumber)
	fmt.Printf("Double BigNumber\t%v\n", *bigNumber)
}
