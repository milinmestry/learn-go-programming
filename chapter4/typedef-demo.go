package main

import "fmt"

var const1 float64 = 273.15

// var const2 float64 = 5 / 9
var constI32 int8 = 32

type fahrenheit float64
type celsius float64
type kelvin float64

func fharToCel(f fahrenheit) celsius {
	return celsius(float64((f - fahrenheit(constI32)) * 5 / 9))
}

func fharToKel(f fahrenheit) celsius {
	return celsius(float64(f-fahrenheit(constI32))*5/9 + const1)
}

func celToFahr(c celsius) fahrenheit {
	return fahrenheit(c*5/9 + celsius(constI32))
}

func celToKel(c celsius) kelvin {
	return kelvin(float64(c) + const1)
}

func main() {
	var c celsius = 32.0
	f := fahrenheit(122)
	fmt.Printf("%.2f \u00b0C = %.2f \u00b0K\n", c, celToKel(c))
	fmt.Printf("%.2f \u00b0F = %.2f \u00b0C\n", f, fharToCel(f))
	// fmt.Printf("const2 =%.2f \n", const2)
}
