package main

import (
	"fmt"
	"math/rand"
)

var list1 = []string{"break", "lake", "go", "right", "strong", "kite", "hello", "mestry"}
var list2 = []string{"fix", "river", "stop", "left", "weak", "flight", "bye", "milin"}

func main() {
	rand.Seed(31)

	for w1, w2 := nextpair(); w1 != "go" && w2 != "stop"; w1, w2 = nextpair() {
		fmt.Printf("Next pair -> [%s\t%s]\n", w1, w2)
	}
}

func nextpair() (w, w2 string) {
	pos := rand.Intn(len(list1))
	return list1[pos], list2[pos]
}
