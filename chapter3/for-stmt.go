package main

import "fmt"

type Curr struct {
	Currency string
	Name     string
	Country  string
	Number   int
}

var currencies = []Curr{
	Curr{"DZD", "Algerian Dinar", "Algeria", 12},
	Curr{"AUD", "Australian Dollar", "Australia", 36},
	Curr{"EUR", "Euro", "Belgium", 978},
	Curr{"CLP", "Chilean Peso", "Chile", 152},
	Curr{"EUR", "Euro", "Greece", 978},
	Curr{"HTG", "Gourde", "Haiti", 332},
	Curr{"HKD", "Hong Kong Dollar", "Hong Koong", 344},
	Curr{"KES", "Kenyan Shilling", "Kenya", 404},
	Curr{"MXN", "Mexican Peso", "Mexico", 484},
	Curr{"USD", "US Dollar", "United States", 840},
	Curr{"EUR", "Euro", "Italy", 978},
}

func main() {
	listCurrencies(16)
}

func listCurrencies(howlong int) {
	i, l := 0, 0

	if howlong > 0 && howlong <= len(currencies) {
		l = howlong
	} else {
		l = len(currencies)
	}

	for i < l {
		fmt.Println(currencies[i])
		i++
	}
}
