package main

import "fmt"

func main() {
	if a := 15; a == 5 {
		fmt.Println("Inside IF block; Value of a is", a)
	} else {
		fmt.Println("Inside ELSE block; Value of a is", a)
	}
	// fmt.Println("Value of a is outside IF block =", a)
}
