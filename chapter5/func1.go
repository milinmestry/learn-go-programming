package main

import "fmt"

func avogadro() float64 {
	return 6.02214129e23
}

func main() {
	fmt.Printf("%%e ~ avogadro() = %e 1/mol\n", avogadro())
	fmt.Printf("%%.2f ~ avogadro() = %.2f\n", avogadro())
}
