package main

import (
	"fmt"
	"math"
)

/*
	Check the given number is a prime number.

	@param int
	@return bool
*/
func isPrime(n int) bool {
	limit := int(math.Sqrt(float64(n)))
	fmt.Printf("limit is %v\n", limit)

	for i := 2; i <= limit; i++ {
		fmt.Printf("{debug} n=%d %% i=%d\n", n, i)
		if (n % i) == 0 {
			return false
		}
	}
	return true
}

func main() {
	n := 79
	fmt.Printf("isPrime(%d)? %v\n", n, isPrime(n))
}
