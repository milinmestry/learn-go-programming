package main

import "fmt"

func add(op0, op1 int) int {
	return op0 + op1
}

func sub(op0, op1 int) int {
	return op0 - op1
}

func main() {
	var opAdd func(int, int) int = add
	// opAdd := add
	opSub := sub

	fmt.Printf("Add(3+6)=%d\n", opAdd(3, 6))
	fmt.Printf("Sub(19-7)=%d\n", opSub(19, 7))
}
