package main

import "fmt"

type Curr struct {
	Currency string
	Name     string
	Country  string
	Number   int
}

var currencies = []Curr{
	Curr{"DZD", "Algerian Dinar", "Algeria", 12},
	Curr{"AUD", "Australian Dollar", "Australia", 36},
	Curr{"EUR", "Euro", "Belgium", 978},
	Curr{"USD", "US Dollar", "United States", 840},
}

func avg(nums ...float64) float64 {
	n := len(nums)
	t := 0.0

	for _, v := range nums {
		t += v
	}
	return t / float64(n)
}

func sum(nums ...float64) float64 {
	t := 0.0

	for _, v := range nums {
		t += v
	}
	return t
}

func mapB2S(s ...bool) string {
	var res string
	for _, v := range s {
		if v == true {
			res += " Yes "
		} else {
			res += " No "
		}
	}
	return res
}

func currNameOnly(strt ...Curr) string {
	var res string
	for _, v := range strt {
		// fmt.Println(v.Name)
		if v.Name != "" {
			res += v.Name + "| "
		}
	}
	return res
}

func main() {
	nums := []float64{1.5, 2.5, 3.75}
	fmt.Printf("avg([%v]) =%.2f\n", nums, avg(nums...))

	fmt.Printf("avg([1, 2.5, 3.75]) =%.2f\n", avg(1, 2.5, 3.75))

	points := []float64{9, 4, 3.7, 7.2, 8.9, 9.2, 10}
	fmt.Printf("sum(%v) = %.2f\n", points, sum(points...))

	fmt.Printf("mapB2S([t, f, f, t, t]) =%v\n", mapB2S(true, false, false, true, true))

	fmt.Printf("Struct demo- currNameOnly() = %v\n", currNameOnly(currencies...))
}
