package main

import "fmt"

func div(op0 int, op1 int, debug bool) (int, int) {
	q := 0
	r := op0
	for r >= op1 {
		if debug {
			fmt.Printf("{debug} q=%d\tr=%d\n", q, r)
		}
		q++
		r = r - op1
	}
	return q, r
}

func main() {
	q, r := div(71, 5, true)
	fmt.Printf("div(71, 5)=> q=%d, r=%d\n", q, r)
}
