package main

import (
	"errors"
	"fmt"
)

var (
	MyErrorInvalid    = errors.New("Invalid argument")
	MyErrorPermission = errors.New("Invalid permission")
	MyErrorFileExists = errors.New("File already exists")
	MyErrorNotExists  = errors.New("File does not exists")
)

func main() {
	fmt.Printf("Error type - [MyErrorInvalid]\t\t%s\n", MyErrorInvalid)
	fmt.Printf("Error type - [MyErrorPermission]\t%s\n", MyErrorPermission)
	fmt.Printf("Error type - [MyErrorFileExists]\t%s\n", MyErrorFileExists)
	fmt.Printf("Error type - [MyErrorNotExists]\t\t%s\n", MyErrorNotExists)
}
