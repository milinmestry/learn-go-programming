package main

import "fmt"

func doSteps(steps ...string) {
	defer fmt.Println("All done!")
	for i, v := range steps {
		defer fmt.Printf("Steps we should do is\t%d) %s\n", i, v)
	}
	fmt.Println("Let's start our defer call.")
}

func main() {
	doSteps(
		"Getup early in the morning",
		"Do meditation",
		"Drink 4 glass of water",
		"Clean your bowel",
		"Do excercise for 30 minutes",
		"Brush your teeth",
		"Have healthy breakfast",
		"Start your office related work",
	)
}
