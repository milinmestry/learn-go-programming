package main

import "fmt"

func half(op0 *float64, debug bool) {
	if debug {
		fmt.Printf("{debug} op0=%f\n", *op0)
	}
	*op0 = *op0 / 2
}

func main() {
	num := 2.807770
	fmt.Printf("num=%f\n", num)
	half(&num, true)
	fmt.Printf("after applying func half() num=%f\n", num)
}
