package main

import "fmt"

var (
	mul = func(num1, num2 int) int {
		return num1 * num2
	}

	sqr = func(num int) int {
		return mul(num, num)
	}
)

func main() {
	fmt.Printf("mul(25, 7) = %d\n", mul(25, 7))
	fmt.Printf("sqr(20) = %d\n", sqr(20))
}
