package main

import "fmt"

func main() {
	fmt.Printf("94(\u00b0F) = %.2f(\u00b0C)\n",
		func(f float64) float64 {
			return (f - 32.0) * (5.0 / 9.0)
		}(94),
	)
}
