package methods

import "fmt"

type gallonn float64

func (g gallonn) quartt() float64 {
	return float64(g * 4)
}

func (g gallonn) half() {
	g = gallonn(g * 0.5)
	fmt.Println("Inside half func the new value is ", g)
}

func (g *gallonn) double() {
	*g = gallonn(*g * 2)
}

func PrintBasicMethod() {
	fmt.Println("-----Basic Methods-----")
	galn := gallonn(6)
	// var gg gallonn = gallonn(0)
	fmt.Printf("For %.2f gallons, quart is %v\n", galn, galn.quartt())
	galn.half() //  This will not update galn var, but its copy
	// fmt.Printf("Half gallonn is %.2f\n", gg)
	fmt.Printf("Half gallonn is %.2f\n", galn)
	galn.double()
	fmt.Printf("Double gallonn is %.2f\n", galn)
}
