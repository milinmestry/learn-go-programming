package oops

import (
	"fmt"
	"math"
)

type ishape interface {
	area() float64
}

type polygon interface {
	ishape
	perim() float64
}

type curved interface {
	ishape
	circinfo() float64
}

func (r *rect2) String() string {
	return fmt.Sprintf(
		"%s[length:%.2f height:%.2f]",
		r.name, r.length, r.height,
	)
}

type circle struct {
	name string
	rad  float64
}

func (c *circle) area() float64 {
	return math.Pi * (c.rad * c.rad)
}

func (c *circle) circinfo() float64 {
	return 2 * math.Pi * c.rad
}

func (c *circle) String() string {
	return fmt.Sprintf(
		"%s[rad: a=%.2f diam=%.2f]",
		c.name, c.rad, (2 * c.rad),
	)
}

// func shapeInfo3(s interface{}) string {
// 	return fmt.Sprintf("Area = %.2f", s.area())
// }

func shapeInfo2(s ishape) string {
	return fmt.Sprintf("Area = %.2f", s.area())
}

func PrintInterface3() {
	fmt.Println("\n-----Interface Implementation 2-----")
	r := &rect2{}
	r.name = "Rectangle Two"
	r.height = 9.0
	r.length = 9.0
	fmt.Println(r, "=>", shapeInfo2(r))

	t := &trianle2{"Triangle Two", 4, 2, 3}
	fmt.Println(t, "=>", shapeInfo2(t))

	c := &circle{"Circle Two", 20}
	fmt.Println(c, "=>", shapeInfo2(c))
}
