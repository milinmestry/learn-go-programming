package oops

import (
	"fmt"
	"math"
)

type shape interface {
	area() float64
	perim() float64
}

type rect2 struct {
	name           string
	height, length float64
}

func (r *rect2) area() float64 {
	return r.height * r.length
}

func (r *rect2) perim() float64 {
	return 2*r.height + 2*r.length
}

type trianle2 struct {
	name                string
	side1, side2, side3 float64
}

func (t *trianle2) area() float64 {
	return 0.5 * (t.side1 * t.side2)
}

func (t *trianle2) perim() float64 {
	return t.side1 + t.side2 + math.Sqrt((t.side1*t.side1)+(t.side2*t.side2))
}

func (t *trianle2) String() string {
	return fmt.Sprintf("%s[sides: side1=%.2f side2=%.2f side3=%.2f]",
		t.name, t.side1, t.side2, t.side3,
	)
}

func shapeInfo(s shape) string {
	return fmt.Sprintf("Area = %.2f", s.area())
}

func PrintInterface2() {
	fmt.Println("\n-----Interface Implementation 1-----")
	r := &rect2{}
	r.name = "Rectangle One"
	r.height = 3.09
	r.length = 4.89
	fmt.Printf("Rectangle details=[name:%s, height:%.2f, length:%.2f]; area of rect=%.2f and perim=%.2f\n", r.name, r.height, r.length, r.area(), r.perim())
	fmt.Println(r, "=>", shapeInfo(r))

	t := &trianle2{"Triangle One", 4, 4, 4}
	fmt.Println(t, "=>", shapeInfo(t))
}
