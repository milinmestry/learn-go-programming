package oops

import "fmt"

type food interface {
	eat()
}

type veggie string

func (v veggie) eat() {
	fmt.Println("Eating veg:", v)
}

type meat string

func (m meat) eat() {
	fmt.Println("Eating food not for human:", m)
}

func eat(f food) {
	veg, ok := f.(veggie)
	if ok {
		if veg == "Okra" {
			fmt.Println("Not eating:", veg)
		} else {
			veg.eat()
		}
		return
	}

	mt, ok := f.(meat)
	if ok {
		if mt == "Beef" {
			fmt.Println("Yuk! Not eating:", mt)
		} else {
			mt.eat()
		}
		return
	}

	fmt.Println("Not eating whatever food it is: ", f)
}

func eatSwitch(f food) {
	switch mosel := f.(type) {
	case veggie:
		switch mosel {
		default:
			mosel.eat()
		case "Okra":
			fmt.Println("Not eating:", mosel)
		}
	case meat:
		switch mosel {
		default:
			mosel.eat()
		case "Beef":
			fmt.Println("Yuk! Not eating:", mosel)
		}
	default:
		fmt.Println("Not eating whatever food it is: ", f)
	}
}

type junkfood string

func (j junkfood) eat() {
	fmt.Println("Eating junkfood:", j)
}

func PrintAssertion() {
	fmt.Println("\n-----Type Assertion 1-----")
	eat(veggie("Carrot"))
	eat(meat("Lamb"))
	eat(veggie("Okra"))
	eat(meat("Beef"))
	eat(junkfood("Burger"))

	fmt.Println("\n-----Type Assertion Switch case-----")
	eatSwitch(veggie("Cucumber"))
	eatSwitch(meat("Beef"))
	eatSwitch(veggie("Tendalie"))
	eatSwitch(meat("Beef again"))
	eatSwitch(junkfood("Pizza"))
}
