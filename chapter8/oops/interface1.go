package oops

import "fmt"

type ivehicle interface {
	operate()
	start()
	stop()
}

type desc struct {
	make  string
	model string
}

type itruck struct {
	desc
	engine
	axels  int
	wheels int
	class  int
}

func (t itruck) drive() {
	fmt.Printf("Driving Truck %s %s, on the go!; ", t.make, t.model)
}

func (e engine) stop() {
	fmt.Printf("Engine stopped!\n")
}

func (t itruck) operate() {
	t.start()
	t.drive()
	t.stop()
}

type iplane struct {
	desc
	engine
	numOfEngines int
	fixedWings   bool
	maxAltitude  int
}

func (p iplane) operate() {
	p.start()
	p.fly()
	p.stop()
}

func (p iplane) fly() {
	fmt.Printf("Flying Aircraft %s %s clear for takeoff! Max altitude=%d; ", p.make, p.model, p.maxAltitude)
}

// func move(v ivehicle) {
// 	v.operate()
// }

func PrintInterface1() {
	fmt.Println("\n-----Interface 1-----")
	t := itruck{
		desc:   desc{"Ford", "F750"},
		engine: engine{GASOLINE + BIO, 700},
		axels:  2,
		wheels: 6,
		class:  3,
	}
	// move(t)
	t.operate()

	p := iplane{}
	p.make = "HondaJet"
	p.model = "HA-849"
	p.fuel = JET
	p.numOfEngines = 2
	p.fixedWings = true
	p.maxAltitude = 43000
	// move(p)
	p.operate()
}
