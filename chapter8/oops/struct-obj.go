package oops

import "fmt"

type fuel int

const (
	GASOLINE fuel = iota
	BIO
	ELECTRIC
	JET
	SONIC
)

type vehicle struct {
	make  string
	model string
}

type engine struct {
	fuel   fuel
	thrust int
}

func (e *engine) start() {
	fmt.Printf("Engine started using engine type %v; ", e.fuel)
}

type truck struct {
	vehicle
	engine
	axels  int
	wheels int
	class  int
}

func newTruck(make, model string) *truck {
	t := &truck{}
	t.make = make
	t.model = model
	return t
}

func (t *truck) drive() {
	fmt.Printf("Truck %s model %s, on the go!\n", t.make, t.model)
}

type plane struct {
	vehicle
	engine
	numOfEngines int
	fixedWings   bool
	maxAltitude  int
}

func newPlane(make, model string) *plane {
	p := &plane{}
	p.make = make
	p.model = model
	return p
}

func (p *plane) fly() {
	fmt.Printf("Aircraft %s %s clear for takeoff! Max altitude=%d\n", p.make, p.model, p.maxAltitude)
}

func PrintPlaneTruck() {
	fmt.Println("\n-----Object Struct-----")
	t := &truck{
		vehicle: vehicle{"Ford", "F750"},
		engine:  engine{GASOLINE + BIO, 700},
		axels:   2,
		wheels:  6,
		class:   3,
	}
	t.start()
	t.drive()

	t2 := newTruck("Tata", "T6383")
	t2.start()
	t2.drive()

	p := &plane{}
	p.make = "HondaJet"
	p.model = "HA-849"
	p.fuel = JET
	p.numOfEngines = 2
	p.fixedWings = true
	p.maxAltitude = 43000
	p.start()
	p.fly()

	p1 := newPlane("ToyotaJet", "TJ-4840")
	p1.fuel = SONIC
	p1.numOfEngines = 3
	p1.fixedWings = true
	p1.maxAltitude = 53000
	p1.start()
	p1.fly()
}
