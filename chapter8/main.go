package main

import (
	"github.com/learning-go-pro-book/chapter6-FooBazz/chapter8/methods"
	"github.com/learning-go-pro-book/chapter6-FooBazz/chapter8/oops"
)

func main() {
	methods.PrintBasicMethod()
	methods.PrintUnits()
	oops.PrintPlaneTruck()
	oops.PrintInterface1()
	oops.PrintInterface2()
	oops.PrintInterface3()
	oops.PrintAssertion()
}
