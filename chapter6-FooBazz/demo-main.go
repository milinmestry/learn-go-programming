package main

import (
	"github.com/learning-go-pro-book/chapter6-FooBazz/chapter6-FooBazz/demo"
	"github.com/learning-go-pro-book/chapter6-FooBazz/chapter6-FooBazz/foo"
)

func main() {
	demo.Fname = "Jayant"
	demo.Lname = "Walke"
	demo.CellNum = "+91 9892028300"
	demo.PrintfInit("")

	foo.FooIt()
}
