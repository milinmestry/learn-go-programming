package demo

import "fmt"

var (
	Fname   string
	Lname   string
	CellNum string
)

func init() {
	Fname = "Add first name"
	Lname = "Add last name"
	CellNum = "Add cell number"
	PrintfInit("Default values are:")
}

func PrintfInit(message string) {
	if message != "" {
		fmt.Println(message)
	}
	fmt.Printf("Fname: %s\n", Fname)
	fmt.Printf("Lname: %s\n", Lname)
	fmt.Printf("CellNum: %s\n", CellNum)
	fmt.Println("-----------")
}
