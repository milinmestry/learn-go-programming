package foo

import (
	"fmt"

	"github.com/learning-go-pro-book/chapter6-FooBazz/chapter6-FooBazz/foo/bazz"
	// "foo/bar/bazz"
	// "github.com/learning-go-pro-book/chapter6-FooBazz/foo/bazz"
)

func FooIt() {
	fmt.Println("Foo!")
	bazz.Qux()
}
